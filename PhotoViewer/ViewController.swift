//
//  ViewController.swift
//  PhotoViewer
//
//  Created by mzgk on 2018/02/19.
//  Copyright © 2018年 mzgk. All rights reserved.
//

import UIKit
import Photos

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    // MARK: - Outlet
    @IBOutlet weak var collectionView: UICollectionView!

    // MARK: - Property
    /// 写真データの取得結果のインスタンスを格納する配列
    var photos = [PHAsset]()
    /// PHAssetインスタンスから画像を取得するため
    let manager = PHImageManager()

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        collectionView.dataSource = self
        collectionView.delegate = self

        // ユーザー許可のダイアログ表示　→ OKタップ後に取得処理（非同期処理）
        PHPhotoLibrary.requestAuthorization { (status: PHAuthorizationStatus) in
            if status == .authorized {
                self.loadPhotos()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - UICollectionViewDataSource
    // セルの数
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }

    // セルの生成
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath)
        // 画像情報の配列から対象の画像情報を取得
        let asset = photos[indexPath.item]
        let width = collectionView.bounds.size.width / 3
        // 画像情報から実際の画像を取得　→ 取得できたらセルに表示する（resultHandler: 非同期処理）
        manager.requestImage(
            for: asset,
            targetSize: CGSize(width: width, height: width),
            contentMode: .aspectFill,
            options: nil,
            resultHandler: { (result, info) in
                if let image = result { // 取得できていれば（Optional Binding）
                    let imageView = cell.viewWithTag(1) as! UIImageView     // Tagでviewを取得し、UIImageViewにダウンキャスト
                    imageView.image = image
                }
        })
        return cell
    }

    // MARK: - UICollectionViewDelegateFlowLayout
    // セルのサイズ
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.size.width / 3
        return CGSize(width: width, height: width)
    }

    // セルマージン（縦）
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }

    // セルマージン（横）
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }

    // MARK: - 写真取得
    /// 写真データの取得
    func loadPhotos() {
        // タイプ：画像を指定して、写真データ（画像そのものではない）を取得
        let result = PHAsset.fetchAssets(with: .image, options: nil)
        // 画像データを配列に格納するためのインデックス構造体を作成
        let indexSet = IndexSet(integersIn: 0 ..< result.count)
        // 画像データを配列に格納
        photos = result.objects(at: indexSet)
        // loadPhotos()はユーザーの許可時に非同期でコールされるので、UI操作をメインスレッドで実行させる
        DispatchQueue.main.sync {
            collectionView.reloadData()
        }
    }
}

